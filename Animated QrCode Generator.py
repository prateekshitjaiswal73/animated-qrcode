import segno
from urllib.request import urlopen

slts_qrcode = segno.make_qr("https://forms.gle/5EWzFYEbGJJqQo899")
slts_qrcode.to_artistic(
    background="video.gif",
    target="animated_qrcode.gif",
    scale=5,
)
